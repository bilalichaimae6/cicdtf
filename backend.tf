terraform {
  backend "s3" {
    bucket         = "cicdbucket101"
    key            = "state"
    region         = "us-east-1"
    dynamodb_table = "backend"
  }
}
